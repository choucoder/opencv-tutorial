import cv2
import numpy as np
import matplotlib.pyplot as plt

def main():
    """Hue: es la característica principal de un color, es su
    tinte, llamas a los colores por su tono, rojo, verde, amarillo,
    etc. Cada color del círculo cromático tiene su particular tono.
    
    Saturation: Es el grado de pureza de un color. A mayor pureza
    mayor saturación. En la práctica los colores suelen mezclarse
    con otros y pierden saturación. Un color mezclado con su
    complementario en diferentes proporciones produce escalas de
    saturación.

    Value: Se refiere a su luminosidad, es decir, a las diferentes
    mezclas que puedes obtener al mezclar un tono con blanco y negro.
    A mayor cantidad de blanco, mayor luminosidad.

    green = np.uint8([[[0,255,0 ]]])
    hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
    """
    # Take frame
    frame = cv2.imread('captura_pdf.png', 1)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Define range of green color in HSV
    upper_green = np.array([75, 255, 255])
    lower_green = np.array([45, 100, 100])

    # Threshold the HSV image to get only green colors
    mask = cv2.inRange(hsv, lower_green, upper_green)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame, frame, mask=mask)

    images = [hsv, mask, res]
    titles = ['HSV', 'MASK', 'RESULT']

    for i in range(len(titles)):
        plt.title(titles[i])
        plt.imshow(images[i], 'gray')
        plt.show()

if __name__ == "__main__":
    main()